/*
    gitops.js
    Methods for managing Git
*/
const dotenv = require('dotenv');
dotenv.config();

const os = require('os');
const home_dir = `${os.homedir()}/`;

const simpleGit = require('simple-git');


module.exports = function (app) {

    app.post('/check_git', async function (req, res) {
        const logged_user = await auth.handleAllReqs(req, res);
        if (logged_user == null) {
            return;
        }
        const git_url = req.body.git_url;
        //Send response
        res.statusCode = 200;
        res.end(JSON.stringify({ msg: "URL added to a checking query" }));

        var check_git = simpleGit(); //simplegit throws error if don't create it each time
        check_git.clone(git_url, home_dir + "temp_git", [], async (err, summary) => {
            if (err) {
                project_status = 'git_check_failed';
                console.log("Cannot clone git: " + err);
            } else {
                project_status = 'git_check_done';
                console.log("Repository was clonned");
            }
            execSync(`rm -rf ` + home_dir + "temp_git");

        });
    });

    //Handles webhooks from Bitbucket, Github etc
    //There is no authorization here but each project has unique deployment_token
    //ToDo: IPs that are not in the whitelist are blocked 
    const test_deployment_token = "hey-key";

    app.post('/deploy/:deployment_key', async function (req, res) {
        
        if (req.params.deployment_key != test_deployment_token){
            res.statusCode = 403;
            res.end(JSON.stringify({ msg: "Invalid deployment key" }));
            return;
        }

        //ToDo: Add a list with white IPs and check IP from where this request is coming

        //Parse request's Payload
        console.log(req.headers);
        console.log(req.body);
        res.statusCode = 200;
        res.end("");
        
    });

    //Parse Bitbucket webhook payload and return Deployment object
    function parseBitbucket(){

    }

    //Parse Github webhook payload and return Deployment object
    function parseGithub(){
        
    }


}