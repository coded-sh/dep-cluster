/*
    haproxy_create_certificate_job_handler.js
    Methods to create TLS certificates
*/

/*
    task dictionary:
    {
        "domain":"",
        "full_chain_cert":""
    }
*/

const dotenv = require('dotenv');
dotenv.config();
const fs = require('fs');

const exec = require('child_process').exec;

async function run(job, logger, finish_handler) {

    logger.info(`Run HAProxy create certificate job with id: ${job.objectId}`);

    const task = job.task;
    if (job.user_data == undefined || task == undefined || task.domain == undefined) {
        finish_handler(job, "The task dictionary hasn't all required values");
        return;
    }

    try {
        //Create a fullchain certificate with name /etc/haproxy/certs/${task.domain}.pem
        const fullchain_cert_path = `/etc/haproxy/certs/${task.domain}.pem`;
        fs.writeFileSync(fullchain_cert_path,job.user_data,{encoding:'utf8',flag:'w'});

        //Load current cert list from /etc/haproxy/crt-list.txt
        //Check if the list includes already /etc/haproxy/certs/${task.domain}.pem, if not add to the list and save to /etc/haproxy/crt-list.txt
        const cert_list_path = `/etc/haproxy/crt-list.txt`;
        const crt_list_content = fs.readFileSync(cert_list_path, 'utf8');
        var certs_list = crt_list_content.split('\n');

        if (certs_list.includes(fullchain_cert_path) == false){
            certs_list.push(fullchain_cert_path);
            certs_list = certs_list.filter(function(e){return e}); 
            //If it's the first certificate in the list, uncomment bind:443 in HAProxy
            if (certs_list.length == 1){
                console.log("updating haproxy");
                const haproxy_cfg_path = '/etc/haproxy/haproxy.cfg';
                var haproxy_cfg = fs.readFileSync(haproxy_cfg_path, 'utf8');
                haproxy_cfg = haproxy_cfg.replace(`# bind *:443 ssl crt-list /etc/haproxy/crt-list.txt`, `bind *:443 ssl crt-list /etc/haproxy/crt-list.txt`);
                haproxy_cfg = haproxy_cfg.replace(`# redirect scheme https code 301 if !{ ssl_fc }`, `redirect scheme https code 301 if !{ ssl_fc }`);
                fs.writeFileSync(haproxy_cfg_path,haproxy_cfg,{encoding:'utf8',flag:'w'});
            }
        }
        fs.writeFileSync(cert_list_path,certs_list.join('\n'),{encoding:'utf8',flag:'w'});

        //Reload HAProxy
        exec(`sudo systemctl restart haproxy`, function (err, stdout, stderr) {
            finish_handler(job, null);
        });

    } catch (error) {
        finish_handler(job, `Cannot create a new ceriticate: ${JSON.stringify(error)}`);
    }
}

module.exports = { run };