/*
    tls_verification_job_handler.js
    Job to verify TLS certificates
*/

/*
    task dictionary:
    {
        "provider":"zerossl",
        "file_validation_url_http":"http://domain.com/.well-known/pki-validation/2449B.txt",
        "file_validation_content":"2B449B722B449B729394793947\ncomodoca.com\n4bad7360c7076ba"
    }
*/

const dotenv = require('dotenv');
dotenv.config();
const fs = require('fs');

async function run(job, logger, finish_handler) {
    logger.info(`Run TLS certificate verifying domain job with id: ${job.objectId}`);

    const task = job.task;
    if (task == undefined || task.provider == undefined || task.file_validation_url_http == undefined  || task.file_validation_content == undefined) {
        finish_handler(job, "The task dictionary hasn't all required values");
        return;
    }

    try {
        //Create a file with name task.file_validation_url_http with content task.file_validation_content
        const file_name = task.file_validation_url_http.substring(task.file_validation_url_http.lastIndexOf('/') + 1)
        const file_verification_path = `./certs/.well-known/pki-validation/${file_name}`;
        fs.writeFileSync(file_verification_path,task.file_validation_content,{encoding:'utf8',flag:'w'});

        finish_handler(job, null);
    } catch (error) {
        finish_handler(job, `Cannot create a new ceriticate: ${JSON.stringify(error)}`);
    }
}

module.exports = { run };